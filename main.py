import tkinter as tk
import random

MATRIX_SIZE = 10

MIN_TIME = 500
MAX_TIME = 2000

WARN_DELAY= 500

MAX_DEPTH = 3

def create_empty_matrix(size):
    return [[' ' for _ in range(size)] for _ in range(size)]

def close_square(x, y):
    if matrix[y][x] != 'X':
        matrix[y][x] = 'X'
        window.after(WARN_DELAY, lambda: canvas.itemconfig(rectangles[y][x], fill='red'))

def warn_player(x, y):
    if matrix[y][x] != 'X':
        label.config(text=f"Закроются: {x}:{y}")
        canvas.itemconfig(rectangles[y][x], fill='yellow')

def random_coords():
    x = random.randint(0, MATRIX_SIZE - 1)
    y = random.randint(0, MATRIX_SIZE - 1)
    return x, y

def is_valid_to_close(x, y):
    if x == 0 or x == MATRIX_SIZE - 1 or y == 0 or y == MATRIX_SIZE - 1:
        return True  # Края считаются закрытыми
    
    def is_row_closed(x, y, dx, dy, depth=0):
        while 0 <= x < MATRIX_SIZE and 0 <= y < MATRIX_SIZE and matrix[y][x] == 'X':
            x += dx
            y += dy
            depth += 1
            if depth > MAX_DEPTH:
                return False
        return depth <= MAX_DEPTH and (x < 0 or x >= MATRIX_SIZE or y < 0 or y >= MATRIX_SIZE)

    # Проверяем, что столбец с одной стороны полностью заполнен и глубина не превышает MAX_DEPTH
    def is_column_closed(x, y, dx, dy, depth=0):
        while 0 <= x < MATRIX_SIZE and 0 <= y < MATRIX_SIZE and matrix[y][x] == 'X':
            x += dx
            y += dy
            depth += 1
            if depth > MAX_DEPTH:
                return False
        return depth <= MAX_DEPTH and (x < 0 or x >= MATRIX_SIZE or y < 0 or y >= MATRIX_SIZE)

    return (is_row_closed(x + 1, y, 1, 0) or is_row_closed(x - 1, y, -1, 0) or
            is_column_closed(x, y + 1, 0, 1) or is_column_closed(x, y - 1, 0, -1))

def close_squares():
    if any(' ' in row for row in matrix):
        x, y = random_coords()
        while not is_valid_to_close(x, y):
            x, y = random_coords()
        
        for _ in range(random.randint(1, 20)):
            warn_player(x, y)
            close_square(x, y)  # Предупредим игрока о закрытии, перед тем как закрыть квадрат

            directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
            random.shuffle(directions)

            for dx, dy in directions:
                new_x, new_y = x + dx, y + dy
                if 0 <= new_x < MATRIX_SIZE and 0 <= new_y < MATRIX_SIZE and matrix[new_y][new_x] != 'X':
                    if is_valid_to_close(new_x, new_y):
                        x, y = new_x, new_y
                    break
        window.update()  # Обновляем окно после закрытия квадратов
        wait_time = random.randint(MIN_TIME, MAX_TIME)
        window.after(wait_time, close_squares)  # Запускаем close_squares снова после задержки

def update_game():
    if any(' ' in row for row in matrix):
        close_squares()
    else:
        close_remaining_squares()

def close_remaining_squares():
    for y in range(1, MATRIX_SIZE - 1):
        for x in range(1, MATRIX_SIZE - 1):
            if matrix[y][x] != 'X':
                close_square(x, y)
                window.update()
                window.after(100)
    label.config(text="Игра окончена")

window = tk.Tk()
window.title("Игра")

canvas = tk.Canvas(window, width=300, height=300)
canvas.pack()

label = tk.Label(window, text="", padx=10, pady=10)
label.pack()

matrix = create_empty_matrix(MATRIX_SIZE)
rectangles = [[canvas.create_rectangle(x * 30, y * 30, (x + 1) * 30, (y + 1) * 30, outline='black', fill='white') for x in range(MATRIX_SIZE)] for y in range(MATRIX_SIZE)]

window.after(1000, update_game)
window.mainloop()